<?php

    /**
     * Class citatcionGraph directed graph.
     * This Class implements a directed graph.
     */
    class citationGraph
    {
        public $graph = array();

        /**
         * add one directed edge, default weight is 1,
         * if edge exists, weight is added on the edge
         * @param $UT_I
         * @param $UT_II
         */
        public function insert($UT_I, $UT_II,$weight=1)
        {
            list($m, $n) = array($UT_I, $UT_II);

            if (!isset($this->graph[$m][$n])) {
                $this->graph[$m][$n] = 0;
            }
            $this->graph[$m][$n] += $weight;
        }
        function __construct(){

        }

        /**
         * @param $dbh
         * @param $sql
         * write all directed edges to database
         */
        public function writeToDb(PDO $dbh,$sql){
            $query = $dbh->prepare($sql);
            $dbh->beginTransaction();
            foreach($this->graph as $m=>$value){
                foreach($value as $n=>$count){
                    $query->execute(array($m,$n,$count));
                }
            }
            $dbh->commit();
        }
    }