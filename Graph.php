<?php

    /**
     * graph/database read/write operations
     */
    $IDX = 'REFID'; // The unique index key on entries, WOK uses REFID, scopus uses eid
    $ATTR = array('author', 'source', 'research_area'); // attributes to create graphs
    $WEIGHT = array(); //  entry/attribute count cache for normalization


    /**
     * get the number of reference of an entry/attribute for normalization
     *
     * @param $_prefix string the attribute
     * @param $_suffix string citation/reference
     * @param $_key string index key (REFID/eid)
     * @param $_value
     * @return mixed
     * @throws Exception
     */
    function getWeight($_prefix, $_suffix, $_key, $_value)
    {
        global $WEIGHT; //cache array storing weight for less db lookup
        if (!isset($WEIGHT[$_prefix][$_value])) {
            $dbh    = init_db();
            $_value = $dbh->quote($_value);
            //entry weight and attr weight use different lookup methods
            if ($_prefix == 'entry')
                $rows = $dbh->query("SELECT * FROM {$_suffix} a WHERE a.{$_key}_I={$_value}");
            else
                $rows = $dbh->query("SELECT a.{$_prefix},r.{$_key}_II FROM {$_prefix} a, {$_suffix} r WHERE a.{$_key}=r.{$_key}_I AND a.{$_prefix}={$_value}");//get weight by crossing attr and entry reference/citation table
            if ($rows === false) throw new \Exception('statement error: ' . $dbh->errorInfo()[2] . "\n");
            $WEIGHT[$_prefix][$_value] = count($rows->fetchAll());
            if ($WEIGHT[$_prefix][$_value] == 0) {
                echo "warning: zero on $_value\n";
                $WEIGHT[$_prefix][$_value] = 1;
            }
        }
        return $WEIGHT[$_prefix][$_value];
    }

    /**
     * Read the database and creates the graph
     *
     * @param PDOStatement $_getCiter
     * @param PDOStatement $_getCitee
     * @param string $_suffix citation/reference
     * @param bool $_normalize
     * @return citationGraph
     * @throws Exception
     */
    function getEntryGraph($_getCiter, $_getCitee, $_suffix = 'reference', $_normalize = false)
    {
        global $IDX;
        $_key = $IDX;
        $entriesGraph = new citationGraph();
        $dbh          = init_db();
        $_getCitee    = $dbh->prepare($_getCitee);
        foreach ($dbh->query($_getCiter) as $row) {
            if (!($_getCitee->execute(array($row["{$_key}_II"])))) throw new \Exception($dbh->errorInfo()[2]);
            $result = $_getCitee->fetchAll();
            //add edge between results
            //note that for every pair of nodes (a,b)
            // a->b and b->a are added
            foreach ($result as $row_i) {
                foreach ($result as $row_ii) {
                    if ($row_i == $row_ii) continue;
                    $weight_I  = $_normalize ? getWeight('entry', $_suffix, $_key, $row_i[0]) : 1;
                    $weight_II = $_normalize ? getWeight('entry', $_suffix, $_key, $row_ii[0]) : 1;
                    $weight    = $weight_I > $weight_II ? 1 / $weight_II : 1 / $weight_I;
                    $entriesGraph->insert($row_i[0], $row_ii[0], $weight);
                }
            }
        }
        return $entriesGraph;
    }

    /**
     * *write graph of entries to database.
     *
     * e.g.
     *
     * @param $_suffix
     * @param bool $_normalize
     */
    function writeEntryGraphToDb($_suffix, $_normalize = false)
    {
        global $IDX;
        $_key = $IDX;
        $suffix = 'co' . $_suffix;
        $dbh    = init_db();
        $dbh->exec("TRUNCATE TABLE entry_{$suffix};");
        $getCiter     = "SELECT {$_key}_II FROM {$_suffix} GROUP BY {$_key}_II HAVING COUNT({$_key}_II)>1";
        $getCitee     = "SELECT {$_key}_I FROM {$_suffix} WHERE {$_key}_II =?";
        $entriesGraph = getEntryGraph($getCiter, $getCitee, $_suffix, $_normalize, $_key);
        $entriesGraph->writeToDb($dbh, "INSERT INTO entry_co{$_suffix}({$_key}_I,{$_key}_II,weight)VALUES(?,?,?)");
    }

    /**
     * wrapper for writenEntryGraphToDb
     * @param bool $_normalize
     */
    function writeEntryCoreferenceGraph($_normalize = false) { writeEntryGraphToDb("reference", $_normalize); }

    /**
     * wrapper for writenEntryGraphToDb
     * @param bool $_normalize
     */
    function writeEntryCocitationGraph($_normalize = false) { writeEntryGraphToDb("citation", $_normalize); }

    /**
     * write graph of a attribute to database.
     *
     * e.g. calling writeGraphToDb('author','citation',false).
     *
     * writes the table author_cocitation.
     *
     * @param $_prefix
     * @param $_suffix
     * @param bool $_normalize
     */
    function writeGraphToDb($_prefix, $_suffix,  $_normalize = false)
    {
        global $IDX;
        $_key = $IDX;
        $dbh    = init_db();
        $suffix = 'co' . $_suffix;
        $dbh->exec("TRUNCATE TABLE {$_prefix}_{$suffix}");
        $attr_query = $dbh->prepare("SELECT {$_prefix} FROM {$_prefix} WHERE {$_key}=?");
        $graph      = new citationGraph();

        foreach ($dbh->query("SELECT * FROM entry_{$suffix} ") as $row) {
            $attr_query->execute(array($row["{$_key}_I"]));
            $attr_I = $attr_query->fetchAll(PDO::FETCH_COLUMN);

            $attr_query->execute(array($row["{$_key}_II"]));
            $attr_II = $attr_query->fetchAll(PDO::FETCH_COLUMN);

            $weight = $row['weight'];
            //note that the edge in table entry_cocitation is already bi-directioned
            foreach ($attr_I as $at_i) {
                foreach ($attr_II as $at_ii) {
                    //                $graph->insert($at_ii, $at_i);
                    $weight_I  = $_normalize ? getWeight($_prefix, $_suffix, $_key, $at_i) : 1;
                    $weight_II = $_normalize ? getWeight($_prefix, $_suffix, $_key, $at_ii) : 1;
                    $weight    = $weight_I > $weight_II ? $weight / $weight_II : $weight / $weight_I;
                    $graph->insert($at_i, $at_ii, $weight);
                }
            }
        }
        $graph->writeToDb($dbh, "INSERT INTO {$_prefix}_{$suffix}({$_prefix}_I,{$_prefix}_II,weight)VALUES(?,?,?)");
    }


    /**
     * A wrapper to writeGraphToDb
     * @param string $_prefix
     * @param bool $_normalize
     */
    function writeCocitationGraph($_prefix = 'author', $_normalize=false) { writeGraphToDb($_prefix, "citation", $_normalize); }

    /**
     * A wrapper to writeGraphToDb
     * @param string $_prefix
     * @param bool $_normalize
     */
    function writeCoreferenceGraph($_prefix = 'author', $_normalize=false) { writeGraphToDb($_prefix, 'reference', $_normalize); }

    /**
     * only extract entries within a year range, construct coreference graphs with them
     *
     * @param $_start start year
     * @param $_end end year
     * @param bool $_normalize normalize the weight on graphs
     */
    function getRangeEntryCoreference($_start, $_end, $_normalize = false)
    {
        global $IDX,$ATTR;
        echo "entry coreference graph.....$_start~$_end...";
        $dbh = init_db();
        $dbh->exec('TRUNCATE TABLE entry_coreference');
        $getCiter     = "SELECT {$IDX}_II FROM reference GROUP BY {$IDX}_II HAVING count({$IDX}_II)>1";
        $getCitee     = "SELECT r.{$IDX}_I FROM reference r,entry e WHERE r.{$IDX}_II =? AND r.{$IDX}_I=e.{$IDX} AND e.published BETWEEN {$_start} AND {$_end}";
        $entriesGraph = getEntryGraph($getCiter, $getCitee, 'reference');
        if (empty($entriesGraph->graph)) {
            echo "empty\n";
        } else {
            $entriesGraph->writeToDb($dbh, "INSERT INTO entry_coreference({$IDX}_I,{$IDX}_II,weight)VALUES(?,?,?)");
            foreach ($ATTR as $attr) writeCoreferenceGraph($attr, $_normalize);
            if ($_normalize) {
                $dbh->exec('TRUNCATE TABLE entry_coreference');
                $normalized_graph = getEntryGraph($getCiter, $getCitee, 'reference', $_normalize);
                $normalized_graph->writeToDb($dbh, "INSERT INTO entry_coreference({$IDX}_I,{$IDX}_II,weight)VALUES(?,?,?)");
                $dir = "normalized_graphs/{$_start}_{$_end}";

            }
            else{
                $dir = "graphs/{$_start}_{$_end}";
            }
            checkOrCreateDir($dir);
            echo "outputting graph...";
            system('python graphGenerator.py');
            system('python csvGenerator.py');
            system('mv entry.csv ' . $dir);
            system('mv *.dl ' . $dir);
            echo "done\n";
        }
    }

?>