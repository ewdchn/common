<?php
    const COOKIESTORAGE = 'cookie.txt';
    $LOG_FILE_NO = 0;
//default option used across CURL requests
    $default_options = array(
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HEADER         => true,
        CURLOPT_FAILONERROR    => true,
        CURLOPT_USERAGENT      => "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.110 Safari/537.36",
        CURLOPT_COOKIEJAR      => COOKIESTORAGE,
        CURLOPT_COOKIEFILE     => COOKIESTORAGE,
    );

    /**
     * handy function to dump crawled html when parsing go wrong
     * @param $_html
     * @param string $_prefix
     */
    function log_html($_html, $_prefix = '')
    {
        global $LOG_FILE_NO;
        file_put_contents($_prefix . '_log_' . $LOG_FILE_NO . '.html', $_html);
        $LOG_FILE_NO++;
    }

    /**
     * check dir existance, writable
     * @param string $_dir name of dir, relative to PWD
     * @return bool
     * @throws \Exception
     */
    function checkOrCreateDir($_dir)
    {
        if (!is_string($_dir)) return false;

        if (file_exists($_dir) && is_dir($_dir)) {
            if (is_writable($_dir))
                return true;
            else throw new \Exception("ERROR: Dir not writable");
        } else {
            mkdir($_dir);
            if (is_writable($_dir))
                return true;
            else throw new \Exception("ERROR: Dir not writable");
        }

    }

    /**
     * Seperate the http headers returned by curl,last being html
     *
     * @param $_page_data
     * @param $_header_cnt
     * @return array array or headers, last being html
     * @throws Exception
     */
    function seperateHeader($_page_data, $_header_cnt)
    {
        $headers = explode("\r\n\r\n", $_page_data, $_header_cnt + 1);
        if (!$headers || empty($headers)) {
            throw new Exception;
        }
        return $headers;
    }

    /**
     * @param string $_cookie_storage file path of cookie file
     * @param string $_site_URL URL of site
     * @return string|bool welcome page or false on failure
     *
     */
    function initSession($_cookie_storage = COOKIESTORAGE, $_site_URL = SITEURL)
    {
        //echo "initializing session...";
        file_put_contents(COOKIESTORAGE, "");
        $options[CURLOPT_URL] = $_site_URL;
        try {
            $response = getPage($options, $_cookie_storage);
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
        //echo "done\n";
        return $response[0];
    }


    /**
     * return webpage of designated URL and options , return content on success, false on ERROR
     *
     * @param null|array $_options
     * @param string $_cookie_storage cookie file path
     * @return array|bool array(response,http_status_code,header_cnt) on success, false on failure
     * @throws \Exception on no response
     *
     */
    function getPage($_options = NULL, $_cookie_storage = COOKIESTORAGE)
    {
        global $default_options;
        $options = $default_options;
        foreach ($_options as $key => $value)
            $options[$key] = $value;
        $options[CURLOPT_COOKIEJAR]  = $_cookie_storage;
        $options[CURLOPT_COOKIEFILE] = $_cookie_storage;

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $tryCnt = 0;
        do {
            if ($tryCnt++ > 5) {
                throw new \Exception("getPage ERROR: No response");
            } else if ($tryCnt > 2) {
                sleep(1);
            }
            $response = curl_exec($ch);
        } while (empty($response));
        $header_cnt = curl_getinfo($ch, CURLINFO_REDIRECT_COUNT) + 1;
        $http_code  = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($http_code != 200) {
            throw new \Exception;
        }
        curl_close($ch);
        return array(html_entity_decode($response), $http_code, $header_cnt);
    }


?>